import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError, tap, timeout } from 'rxjs/operators'
import { environment } from 'environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class RestapiService {
  modelError: Array<any>
  timeout = 5000
  // url = 'http://35.174.137.99:3000/api/'
  url = 'http://localhost:3000/api/'
  // url = 'http://192.168.0.24:3000/api/'


  token: string
  httpOptions: object

  constructor(
    private http: HttpClient
  ) {
    this.httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json'
            })
          }
    // this.state.subscribe(appState => {
    //   const { auth } = appState
    //   if (auth && auth.token) {
    //     this.token = auth.token
    //     this.httpOptions = {
    //       headers: new HttpHeaders({
    //         'Content-Type': 'application/json',
    //         'Authorization': 'Bearer ' + this.token
    //       })
    //     }
    //   }
    // })
  }

  private extractData(res: Response) {
    const body = res
    return body || {}
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (err: any): any => {
      this.modelError = [{ err: err.error }]
      // TODO: send the error to remote logging infrastructure
      console.error(err) // log to console instead
      if (err.status == 401) {
        console.error(this.modelError)
        return this.modelError
      } else {
        console.error(err.status) // log to console instead
        // TODO: better job of transforming error for user consumption
        console.log(`${operation} failed: ${err.message}`)
        console.error(this.modelError)
        return this.modelError
      }
    }
  }

  getApi(path): Observable<any> {
    return this.http.get(this.url + path, this.httpOptions).pipe(
      map(this.extractData))
  }

  getByIdApi(path, param): Observable<any> {
    return this.http.get(this.url + path + '/' + param, this.httpOptions).pipe(
      timeout(this.timeout),
      map(this.extractData))
  }

  getByApi(path, param, value): Observable<any> {
    return this.http.get(this.url + path + '/' + param + '/' + value, this.httpOptions).pipe(
      timeout(this.timeout),
      map(this.extractData))
  }

  postApi(path, payload): Observable<any> {
    return this.http.post<any>(this.url + path, payload, this.httpOptions).pipe(
      timeout(this.timeout),
      tap((model) => {}),
      catchError(this.handleError<any>('Operation: add ' + path))
    )
  }

  updateApi(path, model, param): Observable<any> {
    return this.http.put(this.url + path + '/' + param, JSON.stringify(model), this.httpOptions).pipe(
      timeout(this.timeout),
      tap(_ => {}),
      catchError(this.handleError<any>('Operation: update ' + path))
    )
  }

  deleteApi(path, param): Observable<any> {
    return this.http.delete<any>(this.url + path + '/' + param, this.httpOptions).pipe(
      timeout(this.timeout),
      tap(_ => {}),
      catchError(this.handleError<any>(''))
    )
  }
}
