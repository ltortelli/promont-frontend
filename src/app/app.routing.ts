import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { DeviceDetailComponent } from './pages/device-detail/device-detail.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { MainComponent } from './pages/main/main.component';



const routes: Routes = [
  {
    path: '',
    redirectTo: 'dispositivos',
    pathMatch: 'full',
  },
  { path: 'dispositivo/:serial',      component: DeviceDetailComponent },
  { path: 'user-profile',   component: UserProfileComponent },
  { path: 'dispositivos',     component: MainComponent },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
       useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
