import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeviceModel } from 'app/models/Device.model';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { RestapiService } from 'app/services/restapi.service';
import { DatePipe } from '@angular/common';

export enum Status {
  desconectado = 0,
  teste = 1,
  conectado = 2,
  disjuntor = 3
}



@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.css']
})


export class DeviceDetailComponent implements OnInit {


  sleepDisabled = false;
  lastEstadoEixo: string
  selectedCommands: any = {
    'desconectado': false,
    'conectado': false,
    'teste': false,
    'disjuntor': false
  }

  tableHeader: string[] = [
    'ID',
    'Dispositivo',
    'Última Atualização',
    'IP',
    'Gateway',
    'Porta',
    'Tensão Entrada',
    'Tensão Bateria',
    'Sensor 1',
    'Sensor 2',
    'Sensor 3',
    // 'Alarme 1',
    // 'Alarme 2',
    // 'Alarme 3',
    // 'Alarme 4',
    // 'Alarme 5',
  ]

  device: DeviceModel
  destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)
  refreshInterval: any

  constructor(
    private rest: RestapiService,
    private route: ActivatedRoute,
    public datepipe: DatePipe,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getDevice(params['serial'])
      this.refreshCommandBoard()

    })
  }
  getDevice(serial: any): any {
    this.rest.getApi('device/' + serial).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      const device =  <DeviceModel>res[0]
      device.dtUpdate = this.datepipe.transform(device.dtUpdate, 'dd/MM/yyyy HH:mm:ss')
      this.device = device
      this.madeTableDevices()
    }, error => {
      console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
    })
  }

  madeTableDevices() { }

  changeCommandsInitial() {
    this.lastEstadoEixo = this.device['estadoEixo']
    this.selectedCommands['desconectado'] = this.device['estadoEixo'] === '0' ? true : false
    this.selectedCommands['teste'] = this.device['estadoEixo'] === '1' ? true : false
    this.selectedCommands['conectado'] = this.device['estadoEixo'] === '2' ? true : false
    this.selectedCommands['disjuntor'] = this.device['disjuntor'] === '1' ? true : false
  }

  sendCommand(payload = {}) {
    this.sleepDisabled = true
    payload['serialDevice'] = this.device.serial
    this.rest.postApi('iot/sendCommandDrawer/', payload).pipe(takeUntil(this.destroyed$)).subscribe(async res => {
    }, error => {
      this.sleepDisabled = false
      console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
    })
  }
  refreshCommandBoard() {
    this.refreshInterval = setInterval(() => {
      try {

        this.rest.getApi('device/commandBoard/' + this.device.serial).pipe(takeUntil(this.destroyed$)).subscribe(res => {
          this.device.umidade = res['umidade']
          this.device.temperatura = res['temperatura']
          this.device.disjuntor = res['disjuntor']
          this.device.estadoEixo = res['estadoEixo']
          this.device.dtUpdate = this.datepipe.transform(res['dtUpdate'], 'dd/MM/yyyy HH:mm:ss')
          this.sleepDisabled = false
          this.changeCommandsInitial()
        }, error => {
          this.sleepDisabled = false
          console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
        })
      } catch (e) {
        this.sleepDisabled = false
      }
    }, 5000)


  }

  OnChangeSelectCommand(val: number) {
    switch (val) {
      case 0: { // Desconcetado
        this.selectedCommands[Status[0]] = true
        this.selectedCommands[Status[1]] = false
        this.selectedCommands[Status[2]] = false
        this.lastEstadoEixo = '0'
        this.sendCommand({ 'estadoEixo': '0'})
        break
      }
      case 1: { // Desconcetado
        this.lastEstadoEixo = '1'
        this.selectedCommands[Status[0]] = false
        this.selectedCommands[Status[1]] = true
        this.selectedCommands[Status[2]] = false
        this.sendCommand({ 'estadoEixo': '1' })
        break
      }
      case 2: { // Desconcetado
        this.lastEstadoEixo = '2'
        this.selectedCommands[Status[0]] = false
        this.selectedCommands[Status[1]] = false
        this.selectedCommands[Status[2]] = true
        this.sendCommand({ 'estadoEixo': '2'})
        break
      }
      case 3: {
        this.selectedCommands[Status[3]] = !this.selectedCommands[Status[3]]
        this.sendCommand({'disjuntor': this.selectedCommands[Status[3]] ? '1' : '0' })
      }
    }
    // this.getDevice()

  }

  ngOnDestroy(): void {
    clearInterval(this.refreshInterval)

  }

}
