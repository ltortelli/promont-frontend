import { Component, OnInit } from '@angular/core';
import { RestapiService } from 'app/services/restapi.service';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { DeviceModel } from 'app/models/Device.model';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  devices = new Map()
  refreshInterval: any;
  tableHeader: string[] = [
    'ID',
    'Nome',
    'Disjuntor',
    'Estado Eixo',
    'Ultima Atualização',
    'Detalhes'
  ];


  constructor(
    public datepipe: DatePipe,
    private rest: RestapiService
  ) { }

  ngOnInit() {
    this.refresh()
  }
  refresh() {
    this.refreshInterval = setInterval(() => {
      this.getDevice()
    }, 3000 )
  }

  getValues(): Array<DeviceModel> {
    return Array.from(this.devices.values());
  }

  getDevice() {

    this.rest.getApi('device/').pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res != undefined && res.length != 0) {
        res.forEach(device_res => {
          const device =  <DeviceModel>device_res
          device.dtUpdate = this.datepipe.transform(device.dtUpdate, 'dd/MM/yyyy')
          this.devices.set(device.serial, device)
        });
      }
    }, error => {
      console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
    })

  }


  ngOnDestroy(): void {
    clearInterval(this.refreshInterval)

  }

}
